import React, {useState} from "react"

//import Icons
import {IoMdClose} from 'react-icons/io'
import {CgMenuRight} from 'react-icons/cg'
//import Link 
import {Link} from 'react-router-dom'
//import motion
import {motion} from 'framer-motion'


const menuVar = {
    hidden: {
        x: '100%'
    },
    show: {
        x:0,
        transition: {
            ease: [0.6, 0.01, -0.05, 0.9], 
        },
    },
};


const MobileNav = () => {

    const [openMenu, setOpenMenu] = useState(false);

    return(
        <nav className="text-primary xl:hidden">
            {/* nav open menu */}
            <div onClick={() => setOpenMenu(true)} className="text-3xl cursor-pointer">
                <CgMenuRight />
            </div>
            {/* Mobile Nav */}
            <motion.div 
            variants={menuVar}
            initial='hidden'
            animate={openMenu ? 'show' : ''}
            className="bg-white shadow-2xl w-full absolute top-0 right-0 h-screen z-20 max-w-xs">
                {/* Icons */}
                <div className="text-4xl absolute right-4 top-8 z-30 text-primary cursor-pointer" onClick={() => setOpenMenu(false)}>
                    <IoMdClose />
                </div>

                {/* List of Menu */}
                <ul className="h-full flex flex-col gap-y-8 items-center uppercase text-primary font-primary text-3xl justify-center font-bold">
                    <li>
                        <Link to="/"> Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/contact">Contact</Link>
                    </li>
                    <li>
                        <Link to="/portfolio">Portfolio</Link>
                    </li>
                </ul>


            </motion.div>
        </nav>
    );
};

export default MobileNav;
