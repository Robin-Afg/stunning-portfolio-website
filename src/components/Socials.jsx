import React from "react";

//Import Icons
import {Im500Px, ImTwitter, ImWhatsapp, ImTelegram, ImSkype} from 'react-icons/im'

const Socials = () => {
    return(
        <div className="hidden xl:flex  ml-24">
            <ul className="flex flex-row gap-x-4">
                <li>
                    <a href="https://500px.com" target="_blank" >
                        <Im500Px />
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com" target="_blank" >
                        <ImTwitter />
                    </a>
                </li>
                <li>
                    <a href="https://whatsapp.com" target="_blank" >
                        <ImWhatsapp />
                    </a>
                </li>
                <li>
                    <a href="https://skype.com" target="_blank" >
                        <ImSkype />
                    </a>
                </li>
                <li>
                    <a href="https://telegram.com" target="_blank" >
                        <ImTelegram />
                    </a>
                </li>
            </ul>
        </div>
    );
};

export default Socials;
