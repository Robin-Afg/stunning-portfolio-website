import React from "react";
//import images
import manImg from '../img/header/man.png'
//import link
import {Link} from 'react-router-dom'

//import 
import {motion} from 'framer-motion'
//import transition
import { transition1 } from "../transitions";

const About = () => {
    return(
        <motion.section 
        initial={{opacity: 0, y:'-100%'}}
        animate={{opacity: 1, y: 0}}
        exit={{opacity: 0, y: '-100%'}}
        transition={transition1}
        className="section ">
            <div className="container mx-auto h-full relative">
                {/* text and image */}
                <div className="flex flex-col lg:flex-row h-full items-center justify-center gap-x-24 text-center lg:text-left lg:pt-20">
                    {/* image */}
                    <div className="flex-1 lg:max-h-max order-2 lg:order-none overflow-hidden max-h-96 flex flex-col items-center lg:pt-32 ">
                        <img src={manImg} className="max-w-[60%]" alt="" />
                    </div>
                    {/* content */}
                    <div className="flex-1 pt-36 pb-14 lg:pt-0 lg:w-auto z-10 flex flex-col justify-center items-center lg:items-start">
                        <h1 className="h1">About me</h1>
                        <p className="mb-12 max-w-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam suscipit omnis eligendi dicta soluta debitis! Optio velit ad nesciunt laborum rem. Iste maiores impedit quod esse ex porro totam cum?</p>
                        <Link className="btn">View my Work</Link>
                    </div>
                </div>
            </div>
        </motion.section>
    );
};

export default About;
