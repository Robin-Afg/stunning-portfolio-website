import React from "react";

//import images 
import ManImg from '../img/header/man.png'
//import Link
import {Link} from 'react-router-dom'
//import 
import {motion} from 'framer-motion'
//import transition
import { transition1 } from "../transitions";


const Home = () => {
    return(
        <motion.section 
        initial={{opacity: 0}}
        animate={{opacity: 1}}
        exit={{opacity: 0}}
        transition={transition1}
       
        
         
        className="section">
            <div className="container mx-auto h-full relative">

                
                {/* Text and image wrapper */}
                <motion.div 
                initial={{opacity: 0, y:'-50%'}}
                animate={{opacity: 1, y: 0}}
                exit={{opacity: 0, y: '-50%'}}
                transition={transition1}

                className="flex justify-between lg:pt-34 ">
                    {/* text */}
                    <div className=" pt-36 pb-14 lg:pb-0 flex flex-col justify-center items-center lg:pt-44 ">
                        <span className="h1">
                            Music 
                            <span className=""> & Photography</span>
                        </span>
                        <p className="text-[26px] lg:text-[36px] first-line:font-primary mb-4 lg:mb-12 ">
                            Sydney, AUS
                        </p>
                        <Link to={'/contact'} className="btn mb-[30px]">Hire Me</Link>
                    </div>
                    {/* Image */}
                    <motion.div 
                    initial={{scale: 0}}
                    animate={{scale: 1}}
                    exit={{scale: 0}}
                    transition={transition1}

                    className="flex flex-col max-h-96 lg:pt-[120px] relative left-[20%]" >
                            <motion.img 
                            whileHover={{scale: 1.1}}
                            transition={transition1}
                            src={ManImg} className="max-w-[60%]" alt="man"  /> 
                    </motion.div>
                </motion.div>


            </div>
        </motion.section>
    );
};

export default Home;
