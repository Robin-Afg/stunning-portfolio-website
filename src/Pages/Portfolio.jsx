import React from "react";
//import images
import Image1 from '../img/portfolio/front.jfif'
import Image2 from '../img/portfolio/2.jpg'
import Image3 from '../img/portfolio/3.png'
import Image4 from '../img/portfolio/4.png'

//imoprt link
import {Link} from 'react-router-dom'
//import 
import {motion} from 'framer-motion'
//import transition
import { transition1 } from "../transitions";


const Portfolio = () => {
    return(
        <motion.section 
        initial={{opacity: 0, y:'-50%'}}
        animate={{opacity: 1, y: 0}}
        exit={{opacity: 0, y: '-50%'}}
        transition={transition1}
        className="section">
            <div className="container mx-auto h-full relative">
                <div className="flex flex-col lg:flex-row h-full items-center justify-start gap-x-24 text-center lg:text-left pt-36 lg:pt-48 pb-8">
                    {/* content */}
                    <motion.div 
                     initial={{opacity: 0, y:'-80%'}}
                     animate={{opacity: 1, y: 0}}
                     exit={{opacity: 0, y: '-80%'}}
                     transition={transition1}
                    className="flex flex-col lg:items-start">
                        <h1 className="h1">
                            Portfolio
                        </h1>
                        <p className="mb-12 max-w-sm">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minus tenetur voluptas quo maxime officia unde iste quasi dolorum laboriosam quisquam ab, velit rerum dolores dolore similique. Iure velit voluptatum eum!
                            <br />
                            <br />
                        Make people feel Something !
                        </p>
                        <Link to={'/contact'} className="btn mb-[30px] mx-auto lg:mx-0">Hire ME</Link>
                    </motion.div>

                    {/* Images */}
                    <div className="grid grid-cols-2 lg:gap-2">
                        <div className="img-portfolio-div">
                            <img src={Image1} alt="" className="img-portfolio" />
                        </div>
                        <div className="img-portfolio-div">
                            <img src={Image2} alt="" className="img-portfolio" />
                        </div>
                        <div className="img-portfolio-div">
                            <img src={Image3} alt="" className="img-portfolio" />
                        </div>
                        <div className="img-portfolio-div">
                            <img src={Image4} alt="" className="img-portfolio" />
                        </div>
                        
                    </div>

                </div>
            </div>
        </motion.section>
    );
};

export default Portfolio;
