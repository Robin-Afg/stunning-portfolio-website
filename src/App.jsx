import React from "react";
//import components
import Header from "./components/Header.jsx";
import AnimationRoutes from "./components/AnimationRoutes.jsx";

//import Routes
import { BrowserRouter as Router } from "react-router-dom";

//import motin
import {} from 'framer-motion';
const App = () => {
  return(
    <>
      <Router>
        <Header />
        <AnimationRoutes />
      </Router>
    </>
  );
}

export default App;
